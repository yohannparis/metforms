<?php

// ---- Set include path
$path = '/Users/admin/Development/metforms/';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

// ---- Date
date_default_timezone_set('America/Toronto');
$date = new dateTime('now');
define('DATE', $date->format('Y-m-d H:i'));

// ---- Error Reporting
error_reporting(E_ALL);

/* Utilities */
require_once 'controller/functions.php';
require_once 'controller/pseudocrypt.php';
require_once 'controller/database.php';
require_once 'controller/sessions.php';

/* Classes */
require_once 'controller/user.php';
require_once 'controller/form.php';

/* Router */
ob_start('ob_gzhandler'); // Compress output and turn on buffer
require_once 'controller/route.php';
ob_end_flush(); // Send the Output Buffering
