<?php

// ---- Form
class Form {

	public $id;
	public $link;
	public $user;
	public $invited; // People who has access to the form
	public $status;
	public $type;
	public $brand; // MET or NAC
	public $docket;
	public $client;
	public $name;
	public $content;
	public $date_created;
	public $date_updated;
	public $error; // Error handler
	public $msg; // Error handler

	// -- Show the form
	function get($link)
	{
		global $logged;

		$this->link = $link;
		$this->getForm();

		// We got a proper form
		if(!$this->error){
			$page = $this->getPage();

		// Isn't a form
		} else {
			$page = 'no-form';
		}

		$pageType = 'form';
		include 'view/template.php';
	}

	// -- Update the form
	function post($link)
	{
		global $logged;

		$this->link = $link;
		$this->content = json_encode($_POST);
		$formUpdated = $this->updateForm();

		if ($formUpdated){

			$this->error = false;

			// If everything is fine and the user is logged in,
			// we send him back to the user page.
			if($logged){
				header('Location: /');
				return false;
			} else {
				$this->getForm();

				// Send an email to the creator
				$this->email();

				$page = $this->getPage();
			}

		} else {
			$this->error = true;
			$this->msg = "ERROR_DB";
			$page = $this->getPage();
		}

		$pageType = 'form';
		include 'view/template.php';
	}

	// -- Create a new Form
	function create($user, $client, $name, $docket, $type, $brand)
	{
		// Set the variables
		$this->user = $user;
		$this->client = $client;
		$this->name = $name;
		$this->docket = $docket;
		$this->type = $type;
		$this->brand = $brand;
		$this->error = false; // Everything is fine for the moment

		// Get the DB link
		$database = Database::mysqli();

		// Create the form
		$request = "INSERT INTO forms (user, type, brand, docket, client, name, date_created)
								VALUES ('$this->user', '$this->type', '$this->brand', '$this->docket', '$this->client', '$this->name', SYSDATE());";

		if (mysqli_query($database, $request)){

			// Get the form id
			$this->id = mysqli_insert_id($database);

			// Create a hash for short links from id
			$this->link = PseudoCrypt::hash($this->id);

			// Update the form to include the link
			$request = "UPDATE forms SET link = '".$this->link."' WHERE id = '".$this->id."';";

			if (!mysqli_query($database, $request)){
				$this->error = "ERROR - DB cannot create link"; } // Return error
		}	else { $this->error = "ERROR - DB cannot create form"; } // Return error
	}

	// -- Get all the info from a Form
	private function getForm()
	{
		$request = "SELECT * FROM forms WHERE link = '".$this->link."';";
		$result = mysqli_query(Database::mysqli(), $request);

		// Check if this form exist
		if($result->num_rows>0){
			$this->error = false;

			// Assign the result to the current object
			$result = mysqli_fetch_array($result, MYSQLI_ASSOC);
			$this->id = $result['id'];
			$this->user = $result['user'];
			$this->status = $result['status'];
			$this->type = $result['type'];
			$this->brand = $result['brand'];
			$this->docket = $result['docket'];
			$this->name = $result['name'];
			$this->client = $result['client'];
			$this->content = json_decode($result['content'], true);
			$this->date_created = $result['date_created'];
			$this->date_updated = $result['date_updated'];

			// Get the invited user
			$this->invited = $this->getInvited();

		// the form doesn't exist
		} else {
			// so we tell the user
			$this->error = true;
			$this->msg = "Form do not exist";
		}
	}

	// -- Get the invited user of the form
	public function getInvited(){
		$request = "SELECT users.name, users.email FROM invited, users WHERE invited.form = $this->id AND users.id = invited.user;";
		$result = mysqli_query(Database::mysqli(), $request);

		// Check if users have been invited
		if($result->num_rows>0){
			$results = array();
			while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				array_push($results, $row);
			}
			return $results;

		// otherwise, nobody
		} else {
			// so we tell the user
			return null;
		}
	}

	// -- Get the appropriate page from the type
	private function getPage(){
		switch ($this->type) {

			case 'The One Year Planner':
				return 'one-year-planner';
				break;

			case 'Strategic Projects':
				return 'strategic-projects';
				break;

			case 'Storytelling Brief':
				return 'storytelling-brief';
				break;

			case 'Creative Brief':
				return 'creative-brief';
				break;

			case 'Corporate Video':
				return 'corporate-video';
				break;

			case 'Introduction':
				return 'introduction';
				break;

			case 'Designing Your Website':
				return 'designing-your-website';
				break;

			case 'New Ending':
				return 'new-ending';
				break;

			case 'The Next Chapter':
				return 'next-chapter';
				break;

			case 'The Digital Journey':
				return 'digital-journey';
				break;

			case 'Plot and Structure':
				return 'plot-and-structure';
				break;

			default:
				return 'no-form';
				break;
		}
	}

	// -- Update the Form
	private function updateForm()
	{
		// Get the DB link
		$database = Database::mysqli();

		// Escape the content
		$this->content = $database->real_escape_string($this->content);

		// Prepare the request
		$request = "UPDATE forms SET date_updated = NOW(), content = '".$this->content."' WHERE link = '".$this->link."';";
		$result = mysqli_query($database, $request);
		return $result;
	}

	// -- Display the information of one answer
	public function showAnswer($question, $inputType = null, $inputValue = null)
	{
		// Check if we can prefill some of those answer
		switch ($question) {
			case 'project_docket':
				return $this->docket;
				break;

			case 'project_name':
				return $this->name;
				break;

			case 'project_client':
				return $this->client;
				break;

			default:

				if(isset($this->content[$question])){

					// Switch to know the type of input
					switch ($inputType) {

						// We compare if the answer is equal to the passed value, and if yes we checked that box
						case 'radio':
							return ($this->content[$question] == $inputValue)? 'checked' : '';
							break;

						// We compare if the answer is equal to the passed value, and if yes we checked that box
						case 'checkbox':
							foreach($this->content[$question] as $check) {
								if($check == $inputValue){ return 'checked'; }
							}
							return '';
							break;

						// Basically we just want a text answer
						default:
							return stripslashes($this->content[$question]);
							break;
					}

				} else {
					return false;
				}

				break;
		}
	}

	// -- Delete the Form
	public function delete()
	{
		$request = "DELETE FROM forms WHERE link = '".$this->link."';";
		$result = mysqli_query(Database::mysqli(), $request);
		if ($result){ return true; }
		else { return false; }
	}

	private function email(){

		$user = new User;
		$user->getInformationByID($this->user);

		$to = $user->name.' <'.$user->email.'>';

		$title = '['.$this->docket.'] '.$this->name;

		$headers =  "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		$headers .= "From: MET Forms <developer@metricksystem.com>\r\n";

		$body = '<h3>The form "'.$this->name.'" has been updated:';
		$body .= '<br>http://metrickforms.com/'.$this->link.'</a></h3>';

		mail($to, $title, $body, $headers);
	}
}

// ---- Delete
class Delete {

	function get($link)
	{
		$form = new Form;
		$form->link = $link;
		$form->delete();

		header('Location: /');
	}
}
