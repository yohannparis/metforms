<?php
/* Router
 * Toro PHP - github.com/anandkunal/ToroPHP
 */
include 'vendor/Toro.php';

/* Errors */
ToroHook::add("404",  function() {
	$pageType = 'form';
	$page = 'no-form';
	include 'view/template.php';
});

/* URLs */
Toro::serve(array(
	// User
	"/" => "User",
	"/signout" => "SignOut",

	// Form
	"/([a-zA-Z0-9]+)" => "Form",
	"/([a-zA-Z0-9]+)/delete" => "Delete", // With action (ex: delete)

	// Forget Password
	//"/forget-password" => "forget_password",
	//"/forget-password/([a-zA-Z0-9.@-_]+)/([a-z0-9]+)" => "forget_password",

	// Document download
	//"/([a-zA-Z0-9-_]+.pdf)" => "pdfs",
));
