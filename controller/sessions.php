<?php

// ---- Sessions
ini_set('session.use_only_cookies', true);
session_start();

// ---- Login flag
$logged = false;
if (isset($_SESSION['isLogin']) and $_SESSION['isLogin']){ $logged = true; }

// ---- Session Security
// The Session Id is regenerate every 30 sec to avoid Session theft attack
if (!isset($_SESSION['generated']) || $_SESSION['generated'] < (time()-30)){
	session_regenerate_id();
	$_SESSION['generated'] = time();
}
