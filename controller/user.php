<?php

// ---- User
class User {

	public $id;
	public $name;
	public $email;
	public $admin;
	public $link;

	function __construct()
	{
		$this->error = false; // Everything is fine for the moment
	}

	function get()
	{
		// Create the user object
		$user = new User;

		// Check if the user is logged to redirect him to his user page
		global $logged;
		if($logged){

			$user->getInformation($_SESSION['email']);
			$pageType = 'user';
			$page = 'user';

		// Send the user back to the signin page
		} else {
			$pageType = 'signin';
			$page = 'signin';
		}

		include 'view/template.php';
	}

	function post()
	{
		// Create the user object
		$user = new User;

		global $logged;
		if($logged){

			$user->getInformation($_SESSION['email']);

			// The user want to add another user
			if(isset($_POST['user'])){
				$newUser = new User;
				if(isset($_POST['user-admin'])){ $_POST['user-admin'] = 1; } else { $_POST['user-admin'] = 0; }
				$newUser->registerUser($_POST['user-client'], $_POST['user-name'], $_POST['user-email'], $_POST['user-password'], $_POST['user-admin']);

			// The user want to create a form
			} else {
				$form = new Form;
				$form->create($user->id, $_POST['form-client'], $_POST['form-name'], $_POST['form-docket'], $_POST['form-type'], $_POST['form-brand']);
			}

			$pageType = 'user';
			$page = 'user';

		// ---- The user wants to login or register
		} else {
			$pageType = 'signin';
			$page = 'signin';

			if (isset($_POST['email']) and isset($_POST['password'])){

				// Test if the value aren't empty and email is an email
				if ($_POST['email'] != '' and $_POST['password'] != '') {

					// If the user exist we initialise the Session
					if ($user->existUser($_POST['email'], cleanOutput($_POST['password'])))
					{
						$_SESSION['email'] = $_POST['email'];
						$user->getInformation($_SESSION['email']);

						// The user is now log in
						$_SESSION['isLogin'] = true;
						$logged = true;

						// Set up a Cookie to pre-fill the sign-in form
						setcookie('email', $_SESSION['email'], mktime(0,0,0,1,1,date('Y') + 1));

						// Bring the user page as he is login now
						$pageType = 'user';
						$page = 'user';
					}

					// If the user already exist, but with the wrong password
					else if($user->existUser($_POST['email'])) { $msg = 'ERROR_LOG_IN'; }
				}

				// End test value are empty
				else { $msg = 'ERROR_INFORMATION'; }

			} // End of test Login or register
		}

		include 'view/template.php';
	}

	// ---- User exist, email already used?
	public function existUser($email, $password = null)
	{
		// If the user try to connect
		if ($password != null){
			$request = "SELECT * FROM users WHERE email = '".$email."' AND password = '".md5(Database::$salt.$email.$password)."';";
		}

		// Or just want to check if the email exist
		else { $request = "SELECT * FROM users WHERE email = '".$email."'"; }

		$result = mysqli_query(Database::mysqli(), $request);
		if (mysqli_num_rows($result) > 0){ return true;	}
		else { return false; }
	}

	// ---- Get all the information
	public function getInformation($email)
	{
		$request = "SELECT * FROM users WHERE email = '".$email."';";
		$result = mysqli_query(Database::mysqli(), $request);
		$result = mysqli_fetch_array($result, MYSQLI_ASSOC);

		$this->id = $result['id'];
		$this->email = $result['email'];
		$this->name = $result['name'];
		$this->admin = $result['admin'];
	}

	// ---- Get all the information
	public function getInformationByID($id)
	{
		$request = "SELECT * FROM users WHERE id = $id;";
		$result = mysqli_query(Database::mysqli(), $request);
		$result = mysqli_fetch_array($result, MYSQLI_ASSOC);

		$this->id = $result['id'];
		$this->email = $result['email'];
		$this->name = $result['name'];
		$this->admin = $result['admin'];
	}

	// ---- Get all the forms
	function getForms()
	{
		$request = "SELECT * FROM forms WHERE user = $this->id ORDER BY date_updated DESC, date_created DESC;";
		$result = mysqli_query(Database::mysqli(), $request);
		$results = array();

		while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			array_push($results, $row);
		}

		return $results;
	}

	// ---- Register User
	function registerUser($client = null, $name = null, $email, $password = null, $editor = 0, $admin = 0)
	{
		if (is_null($client)){ $client = ''; }
		if (is_null($name)){ $name = ''; }
		if (!is_null($password)){
			$passwordMD5 = md5(Database::$salt.$email.$password); // Make a MD5 password with SALT word and the email
			$code = md5(Database::$salt.$email); // Make a MD5 code with the email and the SALT word
		} else {
			$passwordMD5 = 0;
			$code = 0;
		}

		$request = "INSERT INTO users (name, email, password, code, editor, admin) VALUES ('$name', '$email', '$passwordMD5', '$code', '$editor', '$admin');";

		if (mysqli_query(Database::mysqli(), $request)){ return true; }
		else { return false; }
	}

	/*
	// ---- Get the email verification code to send an email to the user
	function getEmailVerif($DBlink, $email)
	{
		$requestUser = "SELECT code FROM users WHERE email = '".$email."';";
		$resultUser = mysqli_query($DBlink, $requestUser);
		$result = mysqli_fetch_array($resultUser, MYSQLI_ASSOC);
		return $result['code'];
	}

	// ---- Change the Password
	function changePassword($DBlink, $email, $verificationNumber, $newPassword)
	{
		$request = "UPDATE users SET password = '".md5(Database::$salt.$email.$newPassword)."' WHERE email = '".$email."' AND code = '".$verificationNumber."';";
		$result = mysqli_query($DBlink, $request);
		return $result;
	}

	// ---- Get all the entries done or in progress by the user
	function userEntries($DBlink, $userID)
	{
		$request = "SELECT category, stepProcess, date, entryCode FROM entries WHERE userID = '".$userID."' ORDER BY date ASC;";

		$result = mysqli_query($DBlink, $request);
		$results = array();

		while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			array_push($results, $row);
		}

		return $results;
	}

	// ---- Get all the upload of one entry of the user
	function getUploads($DBlink, $entryCode, $userID)
	{
		$request = "SELECT * FROM uploads WHERE userID = $userID. AND entryCode = '$entryCode' ORDER BY date ASC;";
		$result = mysqli_query($DBlink, $request);
		$results = array();

		while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
			array_push($results, $row);
		}

		return $results;
	}

	// ---- Test if an entryCode is valid with an user.
	function existEntryUser($DBlink, $entryCode, $userID)
	{
		$request = "SELECT * FROM entries WHERE userID = '".$userID."' AND entryCode = '".$entryCode."';";
		$result = mysqli_query($DBlink, $request);
		$results = array();
		if (mysqli_num_rows($result) > 0){ return true; }
		else { return false; }
	}
*/
}

// ---- Sign-out
class SignOut {

	function get() {

		// Empty the Session
		$_SESSION = array();
		unset($user);

		// Delete all information in the Cookies
		if (ini_get("session.use_cookies"))
		{
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
		}

		// Destroy the Session
		session_destroy();
		header('Location: /');
		exit;
	}
}
