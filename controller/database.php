<?php

class Database {

	// Database information
	private static $server = 'external-db.s119975.gridserver.com';
	private static $user = 'db119975_admin';
	private static $password = ':6B..w42Xoto26o';
	private static $database = 'db119975_forms';

	// Some SALT to add some flavour to our MD5 stored on the database.
	public static $salt = 'Gykn^AwoZ/CKvkZypqWFMcCsmxb=qgZjvpHQyv3E4W6GRKRvop';

	// ---- Connection to the Database, hosted on MET Media Temple account.
	public static function mysqli()
	{
		$mysqli = new mysqli(Database::$server, Database::$user, Database::$password, Database::$database);
		if ($mysqli->connect_errno) {
			echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
			return false;
		} else {
			return $mysqli;
		}
	}

}

