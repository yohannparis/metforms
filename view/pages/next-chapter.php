<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1><?=$this->type;?></h1>
</header>

<div class="content">

	<form class="" action="" method="post">

		<section role="project">
			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
			</div>

			<div>
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
				<label>Deadline</label>
				<input type="text" name="project_deadline" value="<?=$this->showAnswer('project_deadline');?>" />
			</div>

		</section>

		<section role="start-with-the-facts">

			<h2>Step 1 - Start with the facts</h2>
			<h3>
				1. Where have you been?
			</h3>

			<fieldset>
				<label for="start-with-the-facts_1-1">Review the history of your brand, its heritage, values and beliefs.</label>
				<textarea name="start-with-the-facts_1-1"><?=$this->showAnswer('start-with-the-facts_1-1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="start-with-the-facts_1-2">List any icons, slogans and symbols of the brand's past that are important to build on.</label>
				<textarea name="start-with-the-facts_1-2"><?=$this->showAnswer('start-with-the-facts_1-2');?></textarea>
			</fieldset>
			<fieldset>
				<label for="start-with-the-facts_1-3">If you had to identify one basic strength of the past what would it be?</label>
				<textarea name="start-with-the-facts_1-3"><?=$this->showAnswer('start-with-the-facts_1-3');?></textarea>
			</fieldset>
			<fieldset class="nextchapter-step1">
				<p style='border-bottom: 1px solid'>
					<label>Where do you stand in the mind of your customer?</label>
					<SPAN style='margin-left:7.5em'>Very</SPAN>
					<SPAN style='margin-left:6em'>Little</SPAN>
					<SPAN style='margin-left:6em'>Not</SPAN></p>
			</fieldset>
			<fieldset class="nextchapter-step1">
				<label for="start-with-the-facts_1-4-1">Different</label>
				<!--Used class = radio to allow modifications to indentation -->
				<div class="radio">
					<span style='margin-left:8.5em'><input type="radio" name="start-with-the-facts_1-4-1" value="Different Very" <?=$this->showAnswer('start-with-the-facts_1-4-1', 'radio', 'Different Very');?>	/></span>
					<span style='margin-left:6em'><input type="radio" name="start-with-the-facts_1-4-1" value="Different Little" 			<?=$this->showAnswer('start-with-the-facts_1-4-1', 'radio', 'Different Little');?>				/></span>
					<span style='margin-left:6em'><input type="radio" name="start-with-the-facts_1-4-1" value="Different Not" 			<?=$this->showAnswer('start-with-the-facts_1-4-1', 'radio', 'Different Not');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step1">
				<label for="start-with-the-facts_1-4-2">Relevant</label>
				<div class="radio">
					<span style='margin-left:8.5em'><input type="radio" name="start-with-the-facts_1-4-2" value="Relevant Very" <?=$this->showAnswer('start-with-the-facts_1-4-2', 'radio', 'Relevant Very');?>	/></span>
					<span style='margin-left:6em'><input type="radio" name="start-with-the-facts_1-4-2" value="Relevant Little" 			<?=$this->showAnswer('start-with-the-facts_1-4-2', 'radio', 'Relevant Little');?>				/></span>
					<span style='margin-left:6em'><input type="radio" name="start-with-the-facts_1-4-2" value="Relevant Not" 			<?=$this->showAnswer('start-with-the-facts_1-4-2', 'radio', 'Relevant Not');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step1">
				<label for="start-with-the-facts_1-4-3">Respected</label>
				<div class="radio">
					<span style='margin-left:8.5em'><input type="radio" name="start-with-the-facts_1-4-3" value="Respected Very" <?=$this->showAnswer('start-with-the-facts_1-4-3', 'radio', 'Respected Very');?>	/></span>
					<span style='margin-left:6em'><input type="radio" name="start-with-the-facts_1-4-3" value="Respected Little" 			<?=$this->showAnswer('start-with-the-facts_1-4-3', 'radio', 'Respected Little');?>				/></span>
					<span style='margin-left:6em'><input type="radio" name="start-with-the-facts_1-4-3" value="Respected Not" 			<?=$this->showAnswer('start-with-the-facts_1-4-3', 'radio', 'Respected Not');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step1">
				<label for="start-with-the-facts_1-4-4">Well Known</label>
				<div class="radio">
					<span style='margin-left:8.5em'><input type="radio" name="start-with-the-facts_1-4-4" value="Well Known Very" <?=$this->showAnswer('start-with-the-facts_1-4-4', 'radio', 'Well Known Very');?>	/></span>
					<span style='margin-left:6em'><input type="radio" name="start-with-the-facts_1-4-4" value="Well Known Little" 			<?=$this->showAnswer('start-with-the-facts_1-4-4', 'radio', 'Well Known Little');?>				/></span>
					<span style='margin-left:6em'><input type="radio" name="start-with-the-facts_1-4-4" value="Well Known Not" 			<?=$this->showAnswer('start-with-the-facts_1-4-4', 'radio', 'Well Known Not');?>			/></span>
				</div>
			</fieldset>
			<fieldset>
				<p style='border-bottom: 1px solid'>List your competitive advantages:</p>
			</fieldset>
			<fieldset>
				<label for="start-with-the-facts_1-5-1">Direct</label>
				<textarea name="start-with-the-facts_1-5-1"><?=$this->showAnswer('start-with-the-facts_1-5-1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="start-with-the-facts_1-5-2">Indirect</label>
				<textarea name="start-with-the-facts_1-5-2"><?=$this->showAnswer('start-with-the-facts_1-5-2');?></textarea>
			</fieldset>
			<hr>
			<fieldset>
				<p>List your competitors :</p>
				<!--<label for="start-with-the-facts_1-6">List your competitors :</label> -->
				<textarea name="start-with-the-facts_1-6"><?=$this->showAnswer('start-with-the-facts_1-6');?></textarea>
			</fieldset>
			<fieldset>
				<p style='border-bottom: 1px solid'>How would your customers respond?</p>
			</fieldset>
			<fieldset>
				<label for="start-with-the-facts_1-7">What is the first thing they think about when they hear your brand's name?</label>
				<textarea name="start-with-the-facts_1-7"><?=$this->showAnswer('start-with-the-facts_1-7');?></textarea>
			</fieldset>
			<h3>
				2. Where is your brand going?
			</h3>

			<fieldset>
				<label for="start-with-the-facts_2-1">What is the vision of your brand? What should it&nbsp;be?</label>
				<textarea name="start-with-the-facts_2-1"><?=$this->showAnswer('start-with-the-facts_2-1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="start-with-the-facts_2-2">How are you going to achieve your vision?</label>
				<textarea name="start-with-the-facts_2-2"><?=$this->showAnswer('start-with-the-facts_2-2');?></textarea>
			</fieldset>
			<fieldset>
				<label for="start-with-the-facts_2-3">What is the basic benefit you are going to&nbsp;provide?</label>
				<textarea name="start-with-the-facts_2-3"><?=$this->showAnswer('start-with-the-facts_2-3');?></textarea>
			</fieldset>

		</section>

		<section role="establish-your-branding-needs">

			<h2>Step 2 - Establish your branding needs</h2>

			<fieldset>
				<label for="establish-your-branding-needs_1">Please check one :</label>
				<div class="radio">
					<span style='margin-left:5em'><input type="radio" name="establish-your-branding-needs_1" value="Refresh" <?=$this->showAnswer('establish-your-branding-needs_1', 'radio', 'Refresh');?>/>    Refresh</span>
					<span style='margin-left:10em'><input type="radio" name="establish-your-branding-needs_1" value="Restart"  <?=$this->showAnswer('establish-your-branding-needs_1', 'radio', 'Restart');?>/>    Restart</span>
				</div>
			</fieldset>
			<br>
			<h3>
				Check off all areas of priority
			</h3>
			<fieldset class="nextchapter-step2">
				<p style='border-bottom: 1px solid'>
					<SPAN style='margin-left:25em'>Refresh</SPAN>
					<SPAN style='margin-left:10.5em'>Restart</SPAN></p>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-1">Basic Strategy</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-1" value="Basic Strategy Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-1', 'radio', 'Basic Strategy Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-1" value="Basic Strategy Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-1', 'radio', 'Basic Strategy Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-2">Merch. Mix</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-2" value="Merch. Mix Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-2', 'radio', 'Merch. Mix Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-2" value="Merch. Mix Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-2', 'radio', 'Merch. Mix Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-3">Store Look</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-3" value="Store Look Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-3', 'radio', 'Store Look Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-3" value="Store Look Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-3', 'radio', 'Store Look Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-4">Service</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-4" value="Service Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-4', 'radio', 'Service Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-4" value="Service Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-4', 'radio', 'Service Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-5">Marketing</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-5" value="Marketing Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-5', 'radio', 'Marketing Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-5" value="Marketing Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-5', 'radio', 'Marketing Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-6">Brand Name</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-6" value="Brand Name Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-6', 'radio', 'Brand Name Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-6" value="Brand Name Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-6', 'radio', 'Brand Name Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-7">Logo</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-7" value="Logo Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-7', 'radio', 'Logo Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-7" value="Logo Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-7', 'radio', 'Logo Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-8">Colour Palette</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-8" value="Colour Palette Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-8', 'radio', 'Colour Palette Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-8" value="Colour Palette Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-8', 'radio', 'Colour Palette Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-9">Positioning Line</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-9" value="Positioning Line Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-9', 'radio', 'Positioning Line Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-9" value="Positioning Line Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-9', 'radio', 'Positioning Line Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-10">Symbols</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-10" value="Symbols Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-10', 'radio', 'Symbols Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-10" value="Symbols Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-10', 'radio', 'Symbols Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-11">Website</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-11" value="Website Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-11', 'radio', 'Website Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-11" value="Website Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-11', 'radio', 'Website Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-12">Social Media</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-12" value="Social Media Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-12', 'radio', 'Social Media Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-12" value="Social Media Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-12', 'radio', 'Social Media Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_2-13">Experiential</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_2-13" value="Experintial Refresh" <?=$this->showAnswer('establish-your-branding-needs_2-13', 'radio', 'Experintial Refresh');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_2-13" value="Experintial Reinvent" 			<?=$this->showAnswer('establish-your-branding-needs_2-13', 'radio', 'Experintial Reinvent');?>			/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<p style='border-bottom: 1px solid'>
					<label>Market/Category Brand Position</label>
					<SPAN style='margin-left:9em'>NOW</SPAN>
					<SPAN style='margin-left:10em'>ASPIRE TO BE</SPAN>
				</p>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_3-1">Leader</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_3-1" value="Leader Now" <?=$this->showAnswer('establish-your-branding-needs_3-1', 'radio', 'Leader Now');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_3-1" value="Leader Aspire To Be" 			<?=$this->showAnswer('establish-your-branding-needs_3-1', 'radio', 'Leader Aspire To Be');?>				/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_3-2">Challenger</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_3-2" value="Challenger Now" <?=$this->showAnswer('establish-your-branding-needs_3-2', 'radio', 'Challenger Now');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_3-2" value="Challenger Aspire To Be" 			<?=$this->showAnswer('establish-your-branding-needs_3-2', 'radio', 'Challenger Aspire To Be');?>				/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_3-3">Middle of the pack</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_3-3" value="Middle of the pack Now" <?=$this->showAnswer('establish-your-branding-needs_3-3', 'radio', 'Middle of the pack Now');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_3-3" value="Middle of the pack Aspire To Be" 			<?=$this->showAnswer('establish-your-branding-needs_3-3', 'radio', 'Middle of the pack Aspire To Be');?>				/></span>
				</div>
			</fieldset>
			<fieldset class="nextchapter-step2">
				<label for="establish-your-branding-needs_3-4">Laggard</label>
				<div class="radio">
					<span style='margin-left:9.5em'><input type="radio" name="establish-your-branding-needs_3-4" value="Laggard Now" <?=$this->showAnswer('establish-your-branding-needs_3-4', 'radio', 'Laggard Now');?>	/></span>
					<span style='margin-left:13.5em'><input type="radio" name="establish-your-branding-needs_3-4" value="Laggard Aspire To Be" 			<?=$this->showAnswer('establish-your-branding-needs_3-4', 'radio', 'Laggard Aspire To Be');?>				/></span>
				</div>
			</fieldset>
			<fieldset>
				<p><i>E.g. The Metrick System is a leader in storytelling.</i></p>
			</fieldset>
			<fieldset>
				<label for="establish-your-branding-needs_4">Write a brief positioning statement that identifies where you want your brand positioned in the marketplace relative to your category and competition.</label>
				<textarea name="establish-your-branding-needs_4"><?=$this->showAnswer('establish-your-branding-needs_4');?></textarea>
			</fieldset>

		</section>

		<section role="segmentation-differentiation-positioning">

			<h2>Step 3 - Segmentation/differentiation/positioning</h2>

			<fieldset>
				<p>What price/quality segment are you going to focus on?</p>
			</fieldset>
			<fieldset>
				<div class="radio">
					<span style='margin-left:2em'><input type="radio" name="segmentation-differentiation-positioning_1" value="Luxury" <?=$this->showAnswer('segmentation-differentiation-positioning_1', 'radio', 'Luxury');?>/>    Luxury</span>
					<span style='margin-left:5em'><input type="radio" name="segmentation-differentiation-positioning_1" value="Up-market"  <?=$this->showAnswer('segmentation-differentiation-positioning_1', 'radio', 'Up-market');?>/>    Up-market</span>
					<span style='margin-left:5em'><input type="radio" name="segmentation-differentiation-positioning_1" value="Mid-market" <?=$this->showAnswer('segmentation-differentiation-positioning_1', 'radio', 'Mid-market');?>/>    Mid-market</span>
					<span style='margin-left:5em'><input type="radio" name="segmentation-differentiation-positioning_1" value="Mass Market"  <?=$this->showAnswer('segmentation-differentiation-positioning_1', 'radio', 'Mass Market');?>/>    Mass market</span>
					<span style='margin-left:5em'><input type="radio" name="segmentation-differentiation-positioning_1" value="Commodity" <?=$this->showAnswer('segmentation-differentiation-positioning_1', 'radio', 'Commodity');?>/>    Commodity</span>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<label for="segmentation-differentiation-positioning_2">What distribution channels are you going to concentrate on?</label>
				<textarea name="segmentation-differentiation-positioning_2"><?=$this->showAnswer('segmentation-differentiation-positioning_2');?></textarea>
			</fieldset>
			<fieldset>
				<label for="segmentation-differentiation-positioning_3">What merchandise/product/service categories are you going to focus on?</label>
				<textarea name="segmentation-differentiation-positioning_3"><?=$this->showAnswer('segmentation-differentiation-positioning_3');?></textarea>
			</fieldset>
			<fieldset>
				<label for="segmentation-differentiation-positioning_4">Write a simple statement that best describes your view of your core customer, i.e., What is it they are looking for?</label>
				<textarea name="segmentation-differentiation-positioning_4"><?=$this->showAnswer('segmentation-differentiation-positioning_4');?></textarea>
			</fieldset>

		</section>

		<section role="designing-your-brand-of-the-future">

			<h2>Step 4 - Designing your brand of the future</h2>

			<fieldset>
				<label for="designing-your-brand-of-the-future_1">What customer motivations are you going to&nbsp;satisfy?</label>
				<textarea name="designing-your-brand-of-the-future_1"><?=$this->showAnswer('designing-your-brand-of-the-future_1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="designing-your-brand-of-the-future_2">What are the unique attributes and benefits you are going to provide?</label>
				<textarea name="designing-your-brand-of-the-future_2"><?=$this->showAnswer('designing-your-brand-of-the-future_2');?></textarea>
			</fieldset>
			<fieldset>
				<label for="designing-your-brand-of-the-future_3">If you were to dream, what could it be?</label>
				<textarea name="designing-your-brand-of-the-future_3"><?=$this->showAnswer('designing-your-brand-of-the-future_3');?></textarea>
			</fieldset>
			<fieldset>
				<label for="designing-your-brand-of-the-future_4">Identify key points of contact between the brand and the customer :</label>
				<textarea name="designing-your-brand-of-the-future_4"><?=$this->showAnswer('designing-your-brand-of-the-future_4');?></textarea>
			</fieldset>

		</section>

		<section role="brand-essence-the-process">

			<h2>Step 5 - Brand essence "The Process"</h2>

			<fieldset>
				<label for="brand-essence-the-process_1">Pick one idea that best describes what you want to own.</label>
				<textarea name="brand-essence-the-process_1"><?=$this->showAnswer('brand-essence-the-process_1');?></textarea>
			</fieldset>
			<label style='width: 100%'>Examples :</label>
			<br>
					<label style='width: 40%; border-top: 1px solid'><b>Jiffy Lube :</b> owns 10 min. oil changes</label>
					<label style='width: 29%; border-top: 1px solid'><b>Google :</b> owns search</label>
					<label style='width: 31%; border-top: 1px solid'><b>Coke :</b> owns "always Coke"</label>
			<br>
					<label style='width: 40%'><b>Staples :</b> owns easy</label>
					<label style='width: 29%'><b>Caterpiller :</b> owns yellow</label>
					<label style='width: 31%'><b>Prudential :</b> owns "the Rock"</label>
			<br>
					<label style='width: 40%'><b>FedEx :</b> owns overnight service</label>
					<label style='width: 29%'><b>Apple :</b> owns innovation</label>
					<label style='width: 31%'><b>Starbucks :</b> owns coffee</label>
			<br>
					<label style='width: 40%'><b>McDonald's :</b> owns fast food</label>
			<!-- Used this tag for page layout-->
			<p></p>
			<hr>

		</section>

		<section role="summary-by-metrick-system">

			<h2>Step 6 - Summary : By Metrick System</h2>

			<fieldset>
				<label for="summary-by-metrick-system_1"><b>Your brand image :</b> What is now in the minds of your customers?</label>
				<textarea name="summary-by-metrick-system_1"><?=$this->showAnswer('summary-by-metrick-system_1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="summary-by-metrick-system_2"><b>Your brand vision :</b> Where your brand aspires to be?</label>
				<textarea name="summary-by-metrick-system_2"><?=$this->showAnswer('summary-by-metrick-system_2');?></textarea>
			</fieldset>
			<fieldset>
				<label for="summary-by-metrick-system_3"><b>Your branding priority :</b> Where your branding activities will focus?</label>
				<textarea name="summary-by-metrick-system_3"><?=$this->showAnswer('summary-by-metrick-system_3');?></textarea>
			</fieldset>
			<fieldset>
				<label for="summary-by-metrick-system_4"><b>Your brand segmentation :</b> Where the brand will compete?</label>
				<textarea name="summary-by-metrick-system_4"><?=$this->showAnswer('summary-by-metrick-system_4');?></textarea>
			</fieldset>
			<fieldset>
				<label for="summary-by-metrick-system_6"><b>Your brand's added value :</b> Beyond the products/services you sell.</label>
				<textarea name="summary-by-metrick-system_6"><?=$this->showAnswer('summary-by-metrick-system_6');?></textarea>
			</fieldset>
			<fieldset>
				<label for="summary-by-metrick-system_7"><b>Your brand's character :</b> The internal values and beliefs of the brand.</label>
				<textarea name="summary-by-metrick-system_7"><?=$this->showAnswer('summary-by-metrick-system_7');?></textarea>
			</fieldset>

		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script type="text/javascript" src="/view/js/textarea.js"></script>
