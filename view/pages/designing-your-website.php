<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1><?=$this->type;?></h1>
</header>

<div class="content">

	<form class="planner" action="" method="post">

		<section role="project">
			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
			</div>

			<div>
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
				<label>Deadline</label>
				<input type="text" name="project_deadline" value="<?=$this->showAnswer('project_deadline');?>" />
			</div>

		</section>

		<section role="audience-targeting">
			<h2>Audience Targeting</h2>

			<fieldset>
				<label for="audience-targeting_1">1. Who are your customers?</label>
				<textarea name="audience-targeting_1"><?=$this->showAnswer('audience-targeting_1');?></textarea>
			</fieldset>

		</section>

		<section role="possibilities-discovery">
			<h2>Possibilities Discovery</h2>
			<h3>
					Your product or service
			</h3>

			<fieldset>
				<label for="possibilities-discovery_1-1">1. Describe your product or service :</label>
				<textarea name="possibilities-discovery_1-1"><?=$this->showAnswer('possibilities-discovery_1-1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_1-2">2. How is your product or service unique?</label>
				<textarea name="possibilities-discovery_1-2"><?=$this->showAnswer('possibilities-discovery_1-2');?></textarea>
			</fieldset>

			<h3>
					The Competitive Enviornment
			</h3>

			<fieldset>
				<label for="possibilities-discovery_2-1">1. Who is your main competitor?</label>
				<textarea name="possibilities-discovery_2-1"><?=$this->showAnswer('possibilities-discovery_2-1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_2-2">2. How does your product or service differ from your competitor's product or service?</label>
				<textarea name="possibilities-discovery_2-2"><?=$this->showAnswer('possibilities-discovery_2-2');?></textarea>
			</fieldset>

			<h3>
					The Story Telling Solution
			</h3>

			<fieldset>
				<label for="possibilities-discovery_3-1">1. What is the purpose of this website?</label>
				<div class="checkbox">
					<span><input type="radio" name="possibilities-discovery_3-1" value="To Educate" <?=$this->showAnswer('possibilities-discovery_3-1', 'radio', 'To Educate');?>	/>To Educate</span>
					<span><input type="radio" name="possibilities-discovery_3-1" value="To Inform" 			<?=$this->showAnswer('possibilities-discovery_3-1', 'radio', 'To Inform');?>				/>To Inform</span>
					<span><input type="radio" name="possibilities-discovery_3-1" value="To Sell" 		<?=$this->showAnswer('possibilities-discovery_3-1', 'radio', 'To Sell');?>		/>To Sell</span>
					<span class="other">
						<input type="radio" name="possibilities-discovery_3-1" value="Other" 		<?=$this->showAnswer('possibilities-discovery_3-1', 'radio', 'Other');?>		/>
							Other: <input type="text" name="possibilities-discovery_3-1-other"  value="<?=$this->showAnswer('possibilities-discovery_3-1-other');?>" />
					</span>
				</div>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-2">2. Why do your customers come to you/choose your product or service?</label>
				<textarea name="possibilities-discovery_3-2"><?=$this->showAnswer('possibilities-discovery_3-2');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-3">3. What are the reasons why someone would not purchase your product or service?</label>
				<textarea name="possibilities-discovery_3-3"><?=$this->showAnswer('possibilities-discovery_3-3');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-4">4. Are there any misconceptions surrounding your product or service?</label>
				<textarea name="possibilities-discovery_3-4"><?=$this->showAnswer('possibilities-discovery_3-4');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-5">5. Do you have branding guidelines to follow? (Colours, Logo, Fonts etc.)</label>
				<div class="checkbox">
					<span><input type="radio" name="possibilities-discovery_3-5" value="Yes" <?=$this->showAnswer('possibilities-discovery_3-5', 'radio', 'Yes');?>/>Yes</span>
					<span><input type="radio" name="possibilities-discovery_3-5" value="No"  <?=$this->showAnswer('possibilities-discovery_3-5', 'radio', 'No');?>/>No</span>
				</div>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-6">6. Do you have an internal person to manage your website?</label>
				<div class="checkbox">
					<span><input type="radio" name="possibilities-discovery_3-6" value="Yes" <?=$this->showAnswer('possibilities-discovery_3-6', 'radio', 'Yes');?>/>Yes</span>
					<span><input type="radio" name="possibilities-discovery_3-6" value="No"  <?=$this->showAnswer('possibilities-discovery_3-6', 'radio', 'No');?>/>No</span>
				</div>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-7">7. Which parts of the website (If any) require a content management system (CMS)?</label>
				<textarea name="possibilities-discovery_3-7"><?=$this->showAnswer('possibilities-discovery_3-7');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-8">8.Do you have existing images or video for your website?</label>
				<div class="checkbox">
					<span><input type="radio" name="possibilities-discovery_3-8" value="Yes" <?=$this->showAnswer('possibilities-discovery_3-8', 'radio', 'Yes');?>/>Yes</span>
					<span><input type="radio" name="possibilities-discovery_3-8" value="No"  <?=$this->showAnswer('possibilities-discovery_3-8', 'radio', 'No');?>/>No</span>
				</div>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-9">9. Will you be providing the text for your website?</label>
				<div class="checkbox">
					<span><input type="radio" name="possibilities-discovery_3-9" value="Yes" <?=$this->showAnswer('possibilities-discovery_3-9', 'radio', 'Yes');?>/>Yes</span>
					<span><input type="radio" name="possibilities-discovery_3-9" value="No"  <?=$this->showAnswer('possibilities-discovery_3-9', 'radio', 'No');?>/>No</span>
				</div>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-10">10. Which social media links do you require?</label>
				<div class="checkbox">
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Facebook"      <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Facebook');?>	/>Facebook</span>
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Linked In" 		 <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Linked In');?>				/>Linked In</span>
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Open Table" 	 <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Open Table');?>			/>Open Table</span>
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Twitter" 			 <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Twitter');?>						/>Twitter</span>
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Pintrest" 		 <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Pintrest');?>							/>Pintrest</span>
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Stumbleupon" 	 <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Stumnleupon');?>					/>Stumnleupon</span>
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Google+" 			 <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Google+');?>			/>Google+</span>
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Youtube/Vimeo" <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Youtube/Vimeo');?>		/>Youtube/Vimeo</span>
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Foursquare" 	 <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Foursquare');?>						/>Foursquare</span>
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Instagram" 		 <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Instagram');?>							/>Instagram</span>
					<span><input type="checkbox" name="possibilities-discovery_3-10[]" value="Tumblr" 			 <?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Tumblr');?>					/>Tumblr</span>
					<span class="other">
						<input type="checkbox" name="possibilities-discovery_3-10[]" value="Other"		<?=$this->showAnswer('possibilities-discovery_3-10', 'checkbox', 'Other');?>	/>
							Other: <input type="text" name="possibilities-discovery_3-10-other" value="<?=$this->showAnswer('possibilities-discovery_3-10-other');?>" />
					</span>
				</div>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-11">11. Which of the following elements will you require?</label>
				<div class="checkbox">
					<span style='margin-left:1em'><input type="checkbox" name="possibilities-discovery_3-11[]" value="Web Hosting" 										<?=$this->showAnswer('possibilities-discovery_3-11', 'checkbox', 'Web Hosting');?>	/>Web Hosting</span>
					<span style='margin-left:6em'><input type="checkbox" name="possibilities-discovery_3-11[]" value="Shopping Cart" 									<?=$this->showAnswer('possibilities-discovery_3-11', 'checkbox', 'Shopping Cart');?>				/>Shopping Cart</span>
					<span style='margin-left:1em'><input type="checkbox" name="possibilities-discovery_3-11[]" value="Paypal/Online Payment" 			    <?=$this->showAnswer('possibilities-discovery_3-11', 'checkbox', 'Paypal/Online Payment');?>			/>Paypal/Online Payment</span>
					<span style='margin-left:6em'><input type="checkbox" name="possibilities-discovery_3-11[]" value="Search Engine Optimization" 		<?=$this->showAnswer('possibilities-discovery_3-11', 'checkbox', 'Search Engine Optimization');?>						/>Search Engine Optimization</span>
					<span style='margin-left:1em'><input type="checkbox" name="possibilities-discovery_3-11[]" value="SSL Certificates" 							<?=$this->showAnswer('possibilities-discovery_3-11', 'checkbox', 'SSL Certificates');?>							/>SSL Certificates</span>
					<span style='margin-left:6em'><input type="checkbox" name="possibilities-discovery_3-11[]" value="Social Media Management" 				<?=$this->showAnswer('possibilities-discovery_3-11', 'checkbox', 'Social Media Management');?>					/>Social Media Management</span>
					<span style='margin-left:1em' class="other">
						<input type="checkbox" name="possibilities-discovery_3-11[]" value="Other"		<?=$this->showAnswer('possibilities-discovery_3-11', 'checkbox', 'Other');?>	/>
							Other: <input type="text" name="possibilities-discovery_3-11-other" value="<?=$this->showAnswer('possibilities-discovery_3-11-other');?>" />
					</span>
				</div>
			</fieldset>

		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script type="text/javascript" src="/view/js/textarea.js"></script>
