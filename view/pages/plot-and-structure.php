<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1><?=$this->type;?></h1>
</header>

<div class="content">

	<form class="project" action="" method="post">

		<section role="project">
			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
			</div>

			<div>
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
				<label>Date</label>
				<input type="text" name="project_date" value="<?=$this->showAnswer('project_date');?>" />
			</div>
		</section>

		<section role="plot-and-structure">
			<fieldset>
				<label>1. What do you want to accomplish?</label>
				<textarea name="plot-and-structure_1"><?=$this->showAnswer('plot-and-structure_1');?></textarea>
			</fieldset>
			<fieldset>
				<label>2. What are the obstacles?</label>
				<textarea name="plot-and-structure_2"><?=$this->showAnswer('plot-and-structure_2');?></textarea>
			</fieldset>
			<fieldset>
				<label>3. What strategies have you already tried?</label>
				<textarea name="plot-and-structure_3"><?=$this->showAnswer('plot-and-structure_3');?></textarea>
			</fieldset>
			<fieldset>
				<label>4. What would be the most satisfying outcome?</label>
				<textarea name="plot-and-structure_4"><?=$this->showAnswer('plot-and-structure_4');?></textarea>
			</fieldset>
		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script type="text/javascript" src="/view/js/textarea.js"></script>
