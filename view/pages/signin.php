<form action="" method="post" class="form-signin">

	<img class="logo" src="/view/images/logo.jpg" alt="The Metrick System" height="59" width="143">

	<label>
		Email
		<?php if (isset($msg) and $msg == 'ERROR_INFORMATION'){ ?><span class="error">is invalid</span><?php } ?>
		<input type="email" name="email" placeholder="name@example.com"
			<?php
				if(isset($_POST['email']) and $_POST['email'] != ''){
					echo 'value="'.$_POST['email'].'"';
				} else if (isset($_COOKIE['email'])){
					echo 'value="'.htmlspecialchars($_COOKIE['email']).'"';
				} else {
					echo "autofocus";
				}
			?>/>
	</label>

	<label>
		Password
		<?php if (isset($msg) and $msg == 'ERROR_LOG_IN'){ ?><span class="error">is invalid</span><?php } ?>
		<input type="text" name="password" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;"
			<?php
				if (
					(isset($_POST['email']) and $_POST['email'] != '')
					or isset($_COOKIE['email'])
				){
					echo "autofocus";
				}
			?>/>
	</label>

	<button type="submit">Login</button>

</form>
