<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1><?=$this->type;?></h1>
</header>

<div class="content">

	<form class="project" action="" method="post">

		<section role="project">
			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
			</div>

			<div>
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
				<label>Date</label>
				<input type="text" name="project_date" value="<?=$this->showAnswer('project_date');?>" />
			</div>
		</section>

		<section role="new-ending">
			<fieldset>
				<label>Briefly describe the experience you’d like to improve and learn from.</label>
				<textarea name="new-ending_experience"><?=$this->showAnswer('new-ending_experience');?></textarea>
			</fieldset>
			<fieldset>
				<label>What’s working?</label>
				<textarea name="new-ending_what-is-working"><?=$this->showAnswer('new-ending_what-is-working');?></textarea>
			</fieldset>
			<fieldset>
				<label>What’s not working?</label>
				<textarea name="new-ending_what-is-not-working"><?=$this->showAnswer('new-ending_what-is-not-working');?></textarea>
			</fieldset>
			<fieldset>
				<label>
					Improvement ideas:<br>
					If you could do this experience over, knowing what you know now, what would you do differently?
				</label>
				<textarea name="new-ending_improvement"><?=$this->showAnswer('new-ending_improvement');?></textarea>
			</fieldset>
			<fieldset>
				<label>Create a series of actions that would produce a much more strategic, successful and satisfying experience in the future.</label>
				<textarea name="new-ending_actions"><?=$this->showAnswer('new-ending_actions');?></textarea>
			</fieldset>
		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script type="text/javascript" src="/view/js/textarea.js"></script>
