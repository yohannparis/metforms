<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1><?=$this->type;?></h1>
	</div>
</header>

<div class="content">

	<form class="corporate-video" action="" method="post">

		<p>
			The Metrick System produces engaging, emotionally-driven corporate videos with one purpose:
			to sell what you do. We produce spectacular movies that are compelling, informative and
			entertaining. We use a unique style of movie-making to create powerful stories that
			communicate your&nbsp;message.
		</p>

		<section role="project">

			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
			</div>

			<div>
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
				<label>Deadline</label>
				<input type="text" name="project_deadline" value="<?=$this->showAnswer('project_deadline');?>" />
			</div>

		</section>

		<section role="audience-targeting">

			<h2>Your Audience</h2>
			<p>
				In this phase of the production we determine your customers and potential target groups.
				We ask you to describe who they are by income, gender, age range and geographic&nbsp;location.
			</p>

			<fieldset>
				<label for="audience-targeting_1">1. Who are your&nbsp;customers?</label>
				<textarea name="audience-targeting_1"><?=$this->showAnswer('audience-targeting_1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="audience-targeting_2">2. What group represents the greatest potential for new or increased&nbsp;business?</label>
				<div class="checkbox">
					<span><input type="checkbox" name="audience-targeting_2[]" value="Existing customers" <?=$this->showAnswer('audience-targeting_2', 'checkbox', 'Existing customers');?>	/>Existing customers</span>
					<span><input type="checkbox" name="audience-targeting_2[]" value="Distributors" 			<?=$this->showAnswer('audience-targeting_2', 'checkbox', 'Distributors');?>				/>Distributors</span>
					<span><input type="checkbox" name="audience-targeting_2[]" value="New Customers" 			<?=$this->showAnswer('audience-targeting_2', 'checkbox', 'New Customers');?>			/>New Customers</span>
					<span><input type="checkbox" name="audience-targeting_2[]" value="Partners" 					<?=$this->showAnswer('audience-targeting_2', 'checkbox', 'Partners');?>						/>Partners</span>
					<span><input type="checkbox" name="audience-targeting_2[]" value="Staff" 							<?=$this->showAnswer('audience-targeting_2', 'checkbox', 'Staff');?>							/>Staff</span>
					<span><input type="checkbox" name="audience-targeting_2[]" value="Operations" 				<?=$this->showAnswer('audience-targeting_2', 'checkbox', 'Operations');?>					/>Operations</span>
					<span><input type="checkbox" name="audience-targeting_2[]" value="Share Holders" 			<?=$this->showAnswer('audience-targeting_2', 'checkbox', 'Share Holders');?>			/>Share Holders</span>
					<span><input type="checkbox" name="audience-targeting_2[]" value="Foreign Markets" 		<?=$this->showAnswer('audience-targeting_2', 'checkbox', 'Foreign Markets');?>		/>Foreign Markets</span>
					<span class="other">
						<input type="checkbox" name="audience-targeting_2" />
							Other: <input type="text" name="audience-targeting_2-other" />
					</span>
				</div>
			</fieldset>
			<fieldset>
				<label for="audience-targeting_3">3. Does the group with the greatest potential know of your products or&nbsp;services?</label>
				<div class="checkbox">
					<span><input type="radio" name="audience-targeting_3" value="Yes" <?=$this->showAnswer('audience-targeting_3', 'radio', 'Yes');?>/>Yes</span>
					<span><input type="radio" name="audience-targeting_3" value="No"  <?=$this->showAnswer('audience-targeting_3', 'radio', 'No');?>/>No</span>
				</div>
			</fieldset>
			<fieldset>
				<label for="audience-targeting_3_reason">Why or why&nbsp;not?</label>
				<textarea name="audience-targeting_3_reason"><?=$this->showAnswer('audience-targeting_3_reason');?></textarea>
			</fieldset>

		</section>

		<section role="possibilities-discovery">

			<h2>Plot and Structure</h2>
			<p>What should happen here? What would be the most satisfying&nbsp;outcome?</p>

			<h3>Your product or service</h3>
			<fieldset>
				<label for="possibilities-discovery_1-1">1. Describe your product or&nbsp;service:</label>
				<textarea name="possibilities-discovery_1-1"><?=$this->showAnswer('possibilities-discovery_1-1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_1-2">2. How is it&nbsp;unique?</label>
				<textarea name="possibilities-discovery_1-2"><?=$this->showAnswer('possibilities-discovery_1-2');?></textarea>
			</fieldset>

			<h3>The competitive environment</h3>
			<fieldset>
				<label for="possibilities-discovery_2-1">1. Whom are we competing&nbsp;against?</label>
				<textarea name="possibilities-discovery_2-1"><?=$this->showAnswer('possibilities-discovery_2-1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_2-2">2. How does your product or service differ from that of your&nbsp;competitor?</label>
				<textarea name="possibilities-discovery_2-2"><?=$this->showAnswer('possibilities-discovery_2-2');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_2-3">3. Which of your offerings are superior to your&nbsp;competitor?</label>
				<textarea name="possibilities-discovery_2-3"><?=$this->showAnswer('possibilities-discovery_2-3');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_2-4">4. Are any of their offerings superior to&nbsp;yours?</label>
				<textarea name="possibilities-discovery_2-4"><?=$this->showAnswer('possibilities-discovery_2-4');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_2-5">5. What objections do you hear from your customers about your&nbsp;offerings?</label>
				<textarea name="possibilities-discovery_2-5"><?=$this->showAnswer('possibilities-discovery_2-5');?></textarea>
			</fieldset>

			<h3>Marketing challenges</h3>
			<fieldset>
				<label for="possibilities-discovery_3-1">1. What problems do you solve for your&nbsp;customers?</label>
				<textarea name="possibilities-discovery_3-1"><?=$this->showAnswer('possibilities-discovery_3-1');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-2">2. What are the reasons someone may not purchase your products or&nbsp;services?</label>
				<textarea name="possibilities-discovery_3-2"><?=$this->showAnswer('possibilities-discovery_3-2');?></textarea>
			</fieldset>
			<fieldset>
				<label for="possibilities-discovery_3-3">3. Are there any misconceptions surrounding your&nbsp;offerings?</label>
				<textarea name="possibilities-discovery_3-3"><?=$this->showAnswer('possibilities-discovery_3-3');?></textarea>
			</fieldset>

		</section>

		<section role="storytelling-matrix">

			<h2>Telling your story</h2>
			<p>
				The age-old practice of storytelling is one of the most effective tools to share ideas.
				Storytellers communicate naturally: analysis might excite the mind but it does not offer
				an easy route to the heart, which is where one must go to make abstract concepts
				meaningful, inspire imagination and motivate&nbsp;action.
			</p>
			<p>
				After we complete the audience analysis and the plot and structure the final draft comes
				to life with our director, video editors and sound engineers creating your corporate video
				to the approved script. Narration is recorded first, then graphics, type and video
				are&nbsp;assembled.
			</p>
			<p>
				Before we edit and mix the master we can tweak your video in case something has been
				forgotten or facts have changed. If we have to rerecord the script there are talent,
				direction and studio&nbsp;costs.
			</p>

		</section>

		<section role="production-and-distribution-uses">

			<h2>Production &amp; distribution uses</h2>
			<div class="checkbox">
				<span><input type="checkbox" name="production-and-distribution-uses[]" value="Live presentation"	<?=$this->showAnswer('production-and-distribution-uses', 'checkbox', 'Live presentation');?>	/>Live presentation</span>
				<span><input type="checkbox" name="production-and-distribution-uses[]" value="Kiosk"							<?=$this->showAnswer('production-and-distribution-uses', 'checkbox', 'Kiosk');?>							/>Kiosk</span>
				<span><input type="checkbox" name="production-and-distribution-uses[]" value="Internet Video"			<?=$this->showAnswer('production-and-distribution-uses', 'checkbox', 'Internet Video');?>			/>Internet Video</span>
				<span><input type="checkbox" name="production-and-distribution-uses[]" value="Website"						<?=$this->showAnswer('production-and-distribution-uses', 'checkbox', 'Website');?>						/>Website</span>
				<span><input type="checkbox" name="production-and-distribution-uses[]" value="Trade show booth"		<?=$this->showAnswer('production-and-distribution-uses', 'checkbox', 'Trade show booth');?>		/>Trade show booth</span>
				<span><input type="checkbox" name="production-and-distribution-uses[]" value="e-mail attachment"	<?=$this->showAnswer('production-and-distribution-uses', 'checkbox', 'e-mail attachment');?>	/>e-mail attachment</span>
				<span><input type="checkbox" name="production-and-distribution-uses[]" value="Social Media"				<?=$this->showAnswer('production-and-distribution-uses', 'checkbox', 'Social Media');?>				/>Social Media</span>
				<span><input type="checkbox" name="production-and-distribution-uses[]" value="Newsletter"					<?=$this->showAnswer('production-and-distribution-uses', 'checkbox', 'Newsletter');?>					/>Newsletter</span>
				<span><input type="checkbox" name="production-and-distribution-uses[]" value="DVD or USB"					<?=$this->showAnswer('production-and-distribution-uses', 'checkbox', 'DVD or USB');?>					/>DVD or USB</span>
			</div>
		</section>

		<section role="">

			<h2>Pre-production process</h2>

			<h3>Production &amp; responsibilities</h3>
			<ol>
				<li>Collateral materials required to produce your video - By client
				<li>The pre-production interview - By client / Metrick System
				<li>The first draft of your video - By Metrick System
				<li>The first draft meeting - By client and Metrick System
				<li>Final draft of the script - By Metrick System
				<li>Production of your video - By Metrick System
			</ol>

			<h4>1. Collateral materials required to produce your video</h4>
			<ul>
				<li>High resolution logo of your firm (ai, and eps formats are ideal).
				<li>Screen shots of your website home page and splash pages.
				<li>Important photos you would like to add to your video (1000px, jpg).
				<li>Any videos about your products or services (320px, wide screen, in Quicktime).
				<li>Press releases.
			</ul>

			<h4>2. The pre-production interview</h4>
			<p>
				The pre-production interview is a set of questions you complete online or over the
				telephone to best describe your company, its products and services. This is the first
				opportunity for us to really understand the ins and outs of your business, industry
				and competitive&nbsp;landscape.
			</p>

			<h4>3. The first draft of your corporate video</h4>
			<p>
				After we receive your answers, we write the first draft of the script, which provides a
				framework for your video. We submit this draft for discussion purposes at the first draft&nbsp;meeting.
			</p>

			<h4>4. The first draft meeting</h4>
			<p>
				We forward the first draft to you before our telephone conference, for you to read and help
				us with additions, revisions, tweaks and possible corrections. This working meeting forms the
				basis for the final draft, which we will forward to you in about a&nbsp;week.
			</p>

			<h4>5. Final draft of the script</h4>
			<p>
				A final draft of the script is written with our collective notes and presented for approval
				with a storyboard outlining a few frames with rough sketches of what you will see on&nbsp;screen.
			</p>

			<h4>6. Production of your corporate video</h4>
			<p>
				The final draft comes to life with our art directors and video editors creating your video
				to the approved script. Narration is recorded first, then we add a music track, sound effects,
				graphics, type and visuals. Before completion, we can make minor revisions to your video at no
				cost. If we have to rerecord the script there are talent, direction	and studio&nbsp;costs.
			</p>

		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script type="text/javascript" src="/view/js/textarea.js"></script>
