<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1><?=$this->type;?></h1>
	</div>
</header>

<div class="content">

	<form class="digital-journey" action="" method="post">

		<section role="project">
			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
			</div>

			<div>
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
				<label>Date</label>
				<input type="text" name="project_date" value="<?=$this->showAnswer('project_date');?>" />
			</div>
		</section>

		<section>
			<?php
				// Rows questions
				$rows[] = array('name' => 'Inspire the customer', 'short' => 'inspire');
				$rows[] = array('name' => 'Make them aware', 'short' => 'aware');
				$rows[] = array('name' => 'Help them decide', 'short' => 'decide');
				$rows[] = array('name' => 'Sell them the product', 'short' => 'sell');
				$rows[] = array('name' => 'Support the customer', 'short' => 'support');
				$rows[] = array('name' => 'Reward loyalty', 'short' => 'reward');

				// Labels
				$labelRow = 'Multiple opportunities across the customer journey';
				$labelColumn = 'Opportunities across digital channels';
			?>

			<span class="label labelColumn"><?=$labelColumn;?></span>
			<table>
				<thead>
					<tr>
						<td></td>
						<td>Physical<br>Stores</td>

						<td class="journey-group-info" data-info="Digital In Store">Store Device</td>
						<td>Own Device</td>

						<td class="journey-group-info" data-info="Digital Out of Store">On the Go</td>
						<td>At Home</td>

						<td>Physical<br>Out Of Store</td>
					</tr>
				</thead>
				<tbody>
					<span class="label labelRow"><?=$labelRow;?></span>
					<?php for ($i=0; $i< count($rows); $i++) { ?>
						<tr>
							<td class="illustrative-examples"><span><?=$rows[$i]['name'];?></span></td>
							<td><textarea name="<?=$rows[$i]['short'];?>-physical-store"><?=$this->showAnswer($rows[$i]['short'].'-physical-store');?></textarea></td>
							<td><textarea name="<?=$rows[$i]['short'];?>-digital-in-store-store-device"><?=$this->showAnswer($rows[$i]['short'].'-digital-in-store-store-device');?></textarea></td>
							<td><textarea name="<?=$rows[$i]['short'];?>-digital-in-store-own-device"><?=$this->showAnswer($rows[$i]['short'].'-digital-in-store-own-device');?></textarea></td>
							<td><textarea name="<?=$rows[$i]['short'];?>-digital-out-of-store-on-the-go"><?=$this->showAnswer($rows[$i]['short'].'-digital-out-of-store-on-the-go');?></textarea></td>
							<td><textarea name="<?=$rows[$i]['short'];?>-digital-out-of-store-at-home"><?=$this->showAnswer($rows[$i]['short'].'-digital-out-of-store-at-home');?></textarea></td>
							<td><textarea name="<?=$rows[$i]['short'];?>-physical-out-of-store"><?=$this->showAnswer($rows[$i]['short'].'-physical-out-of-store');?></textarea></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>

		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script src="/view/js/textarea.js"></script>
