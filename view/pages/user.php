<?php $userForms = $user->getForms(); ?>
<div class="content">

	<nav role="action">
		<a href="/"><img class="logo" src="/model/images/favicon.png" alt="The Metrick System" height="70" width="70"></a>
		<?php if($user->admin){ ?><a href="#new-user" class="top-button">New User</a><?php } ?>
		<?php if(count($userForms)>0){ ?><a href="#new-form" class="top-button">New Form</a><?php } ?>
	</nav>

	<?php if(count($userForms)>0){ ?>
		<section role="forms">
			<h2><?=$user->name;?> forms</h2>
			<table class="forms" cellpadding="0" cellspacing="0">
				<thead><tr>
					<td>Last Update</td>
					<td>Docket</td>
					<td>Client</td>
					<td>Project Name</td>
					<td>Type</td>
					<td>Brand</td>
					<td></td>
					<td></td>
				</tr></thead>
				<?php foreach ($userForms as $userForm) { ?>
					<tr class="form-list">
						<td class="form-date">
							<date><?=date('M j, H:i',strtotime($userForm['date_updated']));?></date></td>
						<td class="form-docket">
							<a href="/<?=$userForm['link'];?>"><?=$userForm['docket'];?></a></td>
						<td class="form-client">
							<a href="/<?=$userForm['link'];?>"><?=$userForm['client'];?></a></td>
						<td class="form-name">
							<a href="/<?=$userForm['link'];?>"><?=$userForm['name'];?></a></td>
						<td class="form-type">
							<?=$userForm['type'];?></td>
						<td class="form-brand">
							<?=$userForm['brand'];?></td>
						<td class="form-link">
							<a href="javascript:void(0)" onclick="copyLink('http://metrickforms.com/<?=$userForm['link'];?>');">Link</a></td>
						<td class="form-delete">
							<a class="form-delete-button" href="/<?=$userForm['link'];?>/delete">Delete</a></td>
					</tr>
				<?php	} ?>
			</table>
		</section>
	<?php } ?>

	<section role="new-form" id="new-form">
		<h2>New form</h2>

		<?php if(isset($form) and $form->error != false){ ?>
			<span class="error"><?=$form->error;?></span>
		<?php } ?>

		<form name="form" action="" method="post">

		<label for="form-client">Client</label>
			<input name="form-client" type="text" placeholder="ex: Elte">
			<label for="form-name">Project Name</label>
			<input name="form-name" type="text" placeholder="ex: Rugs Event">
			<label for="form-docket">Docket</label>
			<input name="form-docket" type="text" placeholder="ex: 11-2345">
			<label for="form-type">Form: </label>
			<select name="form-type">
				<option value="Creative Brief" selected>Creative Brief</option>
				<option value="Introduction">Introduction</option>
				<option value="New Ending">New Ending</option>
				<option value="The Next Chapter">The Next Chapter</option>
				<option value="The Digital Journey">The Digital Journey</option>
				<option value="Storytelling Brief">Storytelling Brief</option>
				<option value="Corporate Video">Corporate Video</option>
				<option value="The One Year Planner">The One Year Planner</option>
				<option value="Strategic Projects">Strategic Projects</option>
				<option value="Plot and Structure">Plot and Structure</option>
				<option value="Designing Your Website">Designing Your Website</option>
			</select>
			<label for="form-brand">Brand: </label>
			<select name="form-brand">
				<option value="MET" selected>Metrick System</option>
				<!-- <option value="NAC">National Advertising Challenge</option> -->
			</select>

			<button class="submit-button" name="button" type="submit">Create form</button>

		</form>
	</section>

	<?php if($user->admin){ ?>
		<section role="new-user" id="new-user">
			<h2>New user</h2>
			<form name="user" action="" method="post">
				<label for="user-name">Name</label>
				<input name="user-name" type="text" placeholder="ex: Developer Metrick">
				<label for="user-email">Email</label>
				<input name="user-email" type="email" placeholder="ex: developer@metricksystem.com">
				<label for="user-password">Password</label>
				<input name="user-password" type="text" placeholder="&bull;&bull;&bull;&bull;&bull;&bull;"/>
				<label for="user-admin">Admin</label><input type="checkbox" name="user-admin">
				<button class="submit-button" name="user" type="submit">Add User</button>
			</form>
		</section>
	<?php } ?>

	<nav role="footer">
		<a href="/signout">Sign out <?=$user->name;?></a>
	</nav>

</div>

<script>
	// Copy link buttons
	function copyLink(link){
		prompt("Press Command(⌘) + C, then Enter", link);
		return false;
	}

	// Delete buttons
	var deleteButtons = document.getElementsByClassName('form-delete-button');
	for (var i = 0, l = deleteButtons.length; i < l; i++) {
		deleteButtons[i].addEventListener('click', function (e) {
			if (!confirm('Are you sure?')) e.preventDefault();
		}, false);
	}
</script>
