<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1><?=$this->type;?></h1>
	</div>
</header>

<div class="content">

	<form class="storytelling" action="" method="post">

		<section role="project">
			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
			</div>

			<div>
				<label>Deadline</label>
				<input type="text" name="project_deadline" value="<?=$this->showAnswer('project_deadline');?>" />
				<label>Deliverables</label>
				<textarea class="fixed" name="project_deliverables"><?=$this->showAnswer('project_deliverables');?></textarea>
			</div>

			<div>
				<label>Mandatories</label>
				<textarea class="fixed" name="project_mandatories"><?=$this->showAnswer('project_mandatories');?></textarea>
			</div>

		</section>

		<section role="about-you">
			<h2>About You</h2>
			<fieldset>
				<label>1. Elevator pitch: You're on an elevator with someone. You have a few seconds to describe your product/service. What do you tell&nbsp;them?</label>
				<textarea name="about-you_1"><?=$this->showAnswer('about-you_1');?></textarea>
			</fieldset>
			<fieldset>
				<label>2. How/Why did your product/service come&nbsp;about?</label>
				<textarea name="about-you_2"><?=$this->showAnswer('about-you_2');?></textarea>
			</fieldset>
			<fieldset>
				<label>3. Why do your customers choose you over the&nbsp;competition?</label>
				<textarea name="about-you_3"><?=$this->showAnswer('about-you_3');?></textarea>
			</fieldset>
			<fieldset>
				<label>4. How would you like this ad/campaign to&nbsp;help?</label>
				<textarea name="about-you_4"><?=$this->showAnswer('about-you_4');?></textarea>
			</fieldset>
			<fieldset>
				<label>5. Is there a specific goal/target for this&nbsp;campaign?</label>
				<textarea name="about-you_5"><?=$this->showAnswer('about-you_5');?></textarea>
			</fieldset>
		</section>

		<section role="customers">
			<h2>Tell Us About Your Customers</h2>
			<fieldset>
				<label>
					1. Imagine your target customer. Then tell us as much as you can about him or her.
					How old? Where do they live? What do they do for work? For play? Family? Job/income?
					The more detail the&nbsp;better.
				</label>
				<textarea name="customers_1"><?=$this->showAnswer('customers_1');?></textarea>
			</fieldset>
			<fieldset>
				<label>
					2. What key piece of information could you give a non-customer to make them
					want to do business with&nbsp;you?
				</label>
				<textarea name="customers_2"><?=$this->showAnswer('customers_2');?></textarea>
			</fieldset>
			<fieldset>
				<label>3. Anything else you want to tell us about your&nbsp;customers?</label>
				<textarea name="customers_3"><?=$this->showAnswer('customers_3');?></textarea>
			</fieldset>
		</section>

		<section role="us-vs-them">
			<h2>Us Vs. Them</h2>
			<div class="columns">
				<fieldset>
					<label>1. What's the main reason people don't choose your&nbsp;product/service?</label>
					<textarea name="us-vs-them_1"><?=$this->showAnswer('us-vs-them_1');?></textarea>
				</fieldset>
				<fieldset>
					<label>2. Who is your competition? (Please name&nbsp;them)</label>
					<textarea name="us-vs-them_2"><?=$this->showAnswer('us-vs-them_2');?></textarea>
				</fieldset>
			</div>
		</section>

		<section role="new-customers">
			<h2>Winning New Customers</h2>
			<fieldset>
				<label>
					1. How do people feel about your product/service now? How do you want them to feel
					as a result of this&nbsp;ad/campaign?
				</label>
				<textarea name="new-customers_1"><?=$this->showAnswer('new-customers_1');?></textarea>
			</fieldset>
			<fieldset>
				<label>
					2. What do we want bloggers, press, twitterati and other influencers to say about
					your&nbsp;product/service?
				</label>
				<textarea name="new-customers_2"><?=$this->showAnswer('new-customers_2');?></textarea>
			</fieldset>
			<fieldset>
				<label>
					3. Any brands we can draw inspiration from? Who's doing what you'd like to do?
					Could be any product or category - could be a brand whose attitude you&nbsp;admire.
				</label>
				<textarea name="new-customers_3"><?=$this->showAnswer('new-customers_3');?></textarea>
			</fieldset>
		</section>

		<section role="finally">
			<h2>And Finally...</h2>
			<fieldset>
				<label>What's on your target customer's&nbsp;mind?</label>
				<textarea name="finally_target"><?=$this->showAnswer('finally_target');?></textarea>
			</fieldset>
			<fieldset>
				<label>Anything you're itching to tell us? Don't hold back. We love&nbsp;details.</label>
				<textarea name="finally_details"><?=$this->showAnswer('finally_details');?></textarea>
			</fieldset>
		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script type="text/javascript" src="/view/js/textarea.js"></script>
