<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1><?=$this->type;?></h1>
	</div>
</header>

<div class="content">

	<form class="planner" action="" method="post">

		<section role="project">
			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
			</div>

			<div>
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
				<label>Date</label>
				<input type="text" name="project_date" value="<?=$this->showAnswer('project_date');?>" />
			</div>
		</section>

		<section>

			<table style='table-layout: fixed'>
				<thead>
					<tr>
						<td>Strengths</td>
						<td class="empty" style='width: 1em'></td>
						<td>Weaknesses</td>
						<td class="empty" style='width: 1em'></td>
						<td>Opportunities</td>
					</tr>
				</thead>
				<tbody>
					<!-- To minimize code and print 10 rows of table -->
					<?php for ($i=1; $i<=10; $i++) { ?>
						<tr>
							<!-- Class used to print No. of row at outer left part. -->
							<td class="data-goal" data-goal="<?=$i;?>">
								<textarea name="<?=$i;?>-strengths"><?=$this->showAnswer($i.'-strengths');?></textarea>
							</td>
							<td class="empty"></td>
							<td>
								 <textarea name="<?=$i;?>-weaknesses"><?=$this->showAnswer($i.'-weaknesses');?></textarea>
							</td>
							<td class="empty"></td>
							<td>
								 <textarea name="<?=$i;?>-opportunities"><?=$this->showAnswer($i.'-opportunities');?></textarea>
							</td>
						</tr>
					<?php }?>
				</tbody>

			</table>

		</section>

		<section>
			<!-- Used table-layout to make adjustments to column widths.-->
			<table style='width :100%; table-layout: fixed'>
				<thead>
					<tr>
						<td style='width :(100/6)%'>Top 3 Strengths</td>
						<td style='width :(100/6)%'>Goal</td>
						<td class="empty" style='width: 1em'></td>
						<td style='width :(100/6)%'>Top 3 Weaknesses</td>
						<td style='width :(100/6)%'>Goal</td>
						<td class="empty" style='width: 1em'></td>
						<td style='width :(100/6)%'>Top 3 Opportunities</td>
						<td style='width :(100/6)%'>Goal</td>
					</tr>
				</thead>
				<tbody>
					<?php for ($i=1; $i<=3; $i++) { ?>
						<tr>
							<td class="data-goal" data-goal="<?=$i;?>">
								<textarea name="<?=$i;?>top-3-strengths"><?=$this->showAnswer($i.'top-3-strengths');?></textarea>
							</td>
							<td>
								 <textarea name="<?=$i;?>top-3-strengths-goal"><?=$this->showAnswer($i.'top-3-strengths-goal');?></textarea>
							</td>
							<td class="empty"></td>
							<td>
								 <textarea name="<?=$i;?>top-3-weaknesses"><?=$this->showAnswer($i.'top-3-weaknesses');?></textarea>
							</td>
							<td>
								 <textarea name="<?=$i;?>top-3-weaknesses-goal"><?=$this->showAnswer($i.'top-3-weaknesses-goal');?></textarea>
							</td>
							<td class="empty"></td>
							<td>
								 <textarea name="<?=$i;?>top-3-opportunities"><?=$this->showAnswer($i.'top-3-opportunities');?></textarea>
							</td>
							<td>
								 <textarea name="<?=$i;?>top-3-opportunities-goal"><?=$this->showAnswer($i.'top-3-opportunities-goal');?></textarea>
							</td>
						</tr>
					<?php }?>
				</tbody>

			</table>

			<p>
				As you proceed to the Next Chapter, consider the following: If we were meeting three years from today,
				looking back what would have to have happened for you to be satisfied with your progress?
			</p>

		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script src="/view/js/textarea.js"></script>
