<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1>Telling Your Story</h1>
</header>

<div class="content">

	<form class="creative" action="" method="post">

		<section role="project">
			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
			</div>

			<div>
				<label>Deadline</label>
				<input type="text" name="project_deadline" value="<?=$this->showAnswer('project_deadline');?>" />
				<label>Deliverables</label>
				<textarea class="fixed" name="project_deliverables"><?=$this->showAnswer('project_deliverables');?></textarea>
			</div>

			<div>
				<label>Mandatories</label>
				<textarea class="fixed" name="project_mandatories"><?=$this->showAnswer('project_mandatories');?></textarea>
			</div>

		</section>

		<section role="about-you">
			<h2>Your Story</h2>
			<fieldset>
				<label>1. What is the business challenge or&nbsp;objective?</label>
				<textarea name="about-you_1"><?=$this->showAnswer('about-you_1');?></textarea>
			</fieldset>
			<fieldset>
				<label>2. Who are we talking to? Who is your target&nbsp;audience?</label>
				<textarea name="about-you_2"><?=$this->showAnswer('about-you_2');?></textarea>
			</fieldset>
			<fieldset>
				<label>3. Are there any category, product or consumer&nbsp;insights?</label>
				<textarea name="about-you_3"><?=$this->showAnswer('about-you_3');?></textarea>
			</fieldset>
			<fieldset>
				<label>4. What is the one main message for this&nbsp;communication?</label>
				<textarea name="about-you_4"><?=$this->showAnswer('about-you_4');?></textarea>
			</fieldset>
			<fieldset>
				<label>5. What are the reasons to&nbsp;believe?</label>
				<textarea name="about-you_5"><?=$this->showAnswer('about-you_5');?></textarea>
			</fieldset>
			<fieldset>
				<label>6. What are the mandatories (or media&nbsp;considerations)?</label>
				<textarea name="about-you_6"><?=$this->showAnswer('about-you_6');?></textarea>
			</fieldset>
		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script type="text/javascript" src="/view/js/textarea.js"></script>
