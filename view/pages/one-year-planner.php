<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1><?=$this->type;?></h1>
	</div>
</header>

<div class="content">

	<form class="planner" action="" method="post">

		<section role="project">
			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
			</div>

			<div>
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
				<label>Date</label>
				<input type="text" name="project_date" value="<?=$this->showAnswer('project_date');?>" />
			</div>
		</section>

		<section role="deadline">
			<h2>Deadlines</h2>
			<div>
				<label>Q1 date</label>
				<input type="text" name="project_Q1date" value="<?=$this->showAnswer('project_Q1date');?>" />
				<label>Q2 date</label>
				<input type="text" name="project_Q2date" value="<?=$this->showAnswer('project_Q2date');?>" />
			</div>

			<div>
				<label>Q3 date</label>
				<input type="text" name="project_Q3date" value="<?=$this->showAnswer('project_Q3date');?>" />
				<label>Q4 date</label>
				<input type="text" name="project_Q4date" value="<?=$this->showAnswer('project_Q4date');?>" />
			</div>
		</section>

		<section>

			<?php
				// Questions on the table
				$questions[1] = 'What are your 10 most crucial one-year goals?';
				$questions[2] = 'Why is this goal so important?';
				$questions[3] = 'Goal';
				$questions[4] = 'Actual Result';

				$nbSelectors = 4; // Number of selectors
				$nbGoals = 10; // Number of goals

				// Selectors to change between Quarters
				$selector = '';
				for ($i=1; $i<=$nbSelectors; $i++) {
					$selector .= '<a class="selector-Q'.$i.'" href="javascript:void(0);">Q'.$i.'</a>';
				}
				$selector = '<nav class="selectors">'.$selector.'</nav>';
			?>

			<?=$selector;?>
			<table>
				<thead>
					<tr>
						<td><?=$questions[1];?></td>
						<td><?=$questions[2];?></td>
						<?php for ($i=1; $i<=$nbSelectors; $i++) { ?>
							<td class="goal-Q<?=$i;?>">Q<?=$i;?> <?=$questions[3];?></td>
							<td class="goal-Q<?=$i;?>">Q<?=$i;?> <?=$questions[4];?></td>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<?php for ($i=1; $i<=$nbGoals; $i++) { ?>
						<tr>
							<td class="data-goal" data-goal="<?=$i;?>">
								<textarea name="<?=$i;?>-name"><?=$this->showAnswer($i.'-name');?></textarea>
							</td>
							<td>
								 <textarea name="<?=$i;?>-why"><?=$this->showAnswer($i.'-why');?></textarea>
							</td>
							<?php for ($j=1; $j<=$nbSelectors; $j++) { ?>
								<td class="goal-Q<?=$j;?>">
								 <textarea name="<?=$i;?>-Q<?=$j;?>-goal"><?=$this->showAnswer($i.'-Q'.$j.'-goal');?></textarea>
								</td>
								<td class="goal-Q<?=$j;?>">
								 <textarea name="<?=$i;?>-Q<?=$j;?>-result"><?=$this->showAnswer($i.'-Q'.$j.'-result');?></textarea>
								</td>
							<?php } ?>
						</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td><?=$questions[1];?></td>
						<td><?=$questions[2];?></td>
						<?php for ($i=1; $i<=$nbSelectors; $i++) { ?>
							<td class="goal-Q<?=$i;?>">Q<?=$i;?> <?=$questions[3];?></td>
							<td class="goal-Q<?=$i;?>">Q<?=$i;?> <?=$questions[4];?></td>
						<?php } ?>
					</tr>
				</tfoot>
			</table>
			<?=$selector;?>

		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script>

	// Go trought the first selectors and display only them
	var firstSelectors = document.querySelectorAll('[class$=Q1]');
	for (var i = firstSelectors.length - 1; i >= 0; i--) {
		firstSelectors[i].classList.add('visible');
	};

	// Go trough each selectors and add events
	for (var i=1; i<=<?=$nbSelectors;?>; i++) {

		// Find the btn at the top and the bottom of the table
		var btnSelectors = document.querySelectorAll('[class^=selector-Q'+i+']');
		for (var j = btnSelectors.length - 1; j >= 0; j--) {

			// Add an event to both
			btnSelectors[j].addEventListener('click', function(){

				// Remove the visible from ALL selectors
				var activeSelectors = document.querySelectorAll('.visible');
				for (var k = activeSelectors.length - 1; k >= 0; k--) {

					// Made them invisible
					activeSelectors[k].classList.remove('visible');
				}

				// Find the selectors related to this button by getting it's class
				var selectors = document.querySelectorAll('[class*=Q'+this.classList[0].slice(-1)+']');
				for (var k = selectors.length - 1; k >= 0; k--) {

					// Made them visible
					selectors[k].classList.add('visible');
				}
			});
		}
	};

</script>
<script src="/view/js/textarea.js"></script>
