<header>
	<div class="content">
		<a href="/"><img class="logo" src="/view/images/logo-white.png" alt="The Metrick System" height="59" width="143"></a>
		<span form="<?=$this->link;?>"><?=date('M j Y, H:i',strtotime($this->date_updated));?></span>
		<h1><?=$this->type;?></h1>
	</div>
</header>

<div class="content">

	<form class="strategic" action="" method="post">

		<section role="project">
			<h2>Project</h2>

			<div>
				<label>Client</label>
				<input type="text" name="project_client" value="<?=$this->showAnswer('project_client');?>" disabled />
				<label>Project</label>
				<input type="text" name="project_name" value="<?=$this->showAnswer('project_name');?>" disabled />
			</div>

			<div>
				<label>Docket #</label>
				<input type="text" name="project_docket" value="<?=$this->showAnswer('project_docket');?>" disabled />
				<label>Date</label>
				<input type="text" name="project_date" value="<?=$this->showAnswer('project_date');?>" />
			</div>
		</section>

		<section>

			<?php
				// Questions on the table
				$questions[1] = 'Obstacles';
				$questions[2] = 'Strategies';
				$questions[3] = 'Results';
				$nbGoals = 10; // Number of goals
			?>

			<table>
				<thead>
					<tr>
						<td><?=$questions[1];?></td>
						<td><?=$questions[2];?></td>
						<td><?=$questions[3];?></td>
					</tr>
				</thead>
				<tbody>
					<?php for ($i=1; $i<=$nbGoals; $i++) { ?>
						<tr>
							<td class="data-goal" data-goal="<?=$i;?>">
								<textarea name="<?=$i;?>-obstacles"><?=$this->showAnswer($i.'-strategies');?></textarea>
							</td>
							<td>
								 <textarea name="<?=$i;?>-strategies"><?=$this->showAnswer($i.'-strategies');?></textarea>
							</td>
							<td>
								 <textarea name="<?=$i;?>-results"><?=$this->showAnswer($i.'-results');?></textarea>
							</td>
						</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td><?=$questions[1];?></td>
						<td><?=$questions[2];?></td>
						<td><?=$questions[3];?></td>
					</tr>
				</tfoot>
			</table>

		</section>

		<section class="montly">

			<?php
				// Questions on the table
				$questions[1] = 'Strategies';
				$questions[2] = 'Who is involved';
				$questions[3] = 'Implementations Deadline';
				$questions[4] = 'Month';

				$nbSelectors = 12; // Number of selectors
				$nbGoals = 10; // Number of goals

				// Selectors to change between Quarters
				$selector = '';
				for ($i=1; $i<=$nbSelectors; $i++) {
					$selector .= '<a class="selector-M'.sprintf('%02d', $i).'" href="javascript:void(0);">'.$i.'</a>';
				}
				$selector = '<nav class="selectors">'.$selector.'</nav>';
			?>

			<?=$selector;?>
			<table>
				<thead>
					<tr>
						<td><?=$questions[1];?></td>
						<td><?=$questions[2];?></td>
						<td><?=$questions[3];?></td>
						<?php for ($i=1; $i<=$nbSelectors; $i++) { ?>
							<td class="goal-M<?=sprintf('%02d', $i);?>"><?=$questions[4];?> <?=$i;?></td>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<?php for ($i=1; $i<=$nbGoals; $i++) { ?>
						<tr>
							<td class="data-goal" data-goal="<?=$i;?>">
								<textarea name="goal<?=$i;?>-strategies"><?=$this->showAnswer('goal'.$i.'-strategies');?></textarea>
							</td>
							<td>
								 <textarea name="goal<?=$i;?>-who"><?=$this->showAnswer('goal'.$i.'-who');?></textarea>
							</td>
							<td>
								 <textarea name="goal<?=$i;?>-deadline"><?=$this->showAnswer('goal'.$i.'-deadline');?></textarea>
							</td>
							<?php for ($j=1; $j<=$nbSelectors; $j++) { ?>
								<td class="goal-M<?=sprintf('%02d', $j);?>">
								 <textarea name="goal<?=$i;?>-M<?=$j;?>-result"><?=$this->showAnswer('goal'.$i.'-M'.$j.'-result');?></textarea>
								</td>
							<?php } ?>
						</tr>
					<?php } ?>
				</tbody>
				<tfoot>
					<tr>
						<td><?=$questions[1];?></td>
						<td><?=$questions[2];?></td>
						<td><?=$questions[3];?></td>
						<?php for ($i=1; $i<=$nbSelectors; $i++) { ?>
							<td class="goal-M<?=sprintf('%02d', $i);?>"><?=$questions[4];?> <?=$i;?></td>
						<?php } ?>
					</tr>
				</tfoot>
			</table>
			<?=$selector;?>

		</section>

		<button class="form-buttom" name="button" type="submit">Save</button>

	</form>

</div>

<script>

	// Go trought the first selectors and display only them
	var firstSelectors = document.querySelectorAll('[class$=M01]');
	for (var i = firstSelectors.length - 1; i >= 0; i--) {
		firstSelectors[i].classList.add('visible');
	};

	// Go trough each selectors and add events
	for (var i=1; i<=<?=$nbSelectors;?>; i++) {

		// Find the btn at the top and the bottom of the table
		if (i<10){ i = '0'+i; }
		var btnSelectors = document.querySelectorAll('[class^=selector-M'+i+']');
		for (var j = btnSelectors.length - 1; j >= 0; j--) {

			// Add an event to both
			btnSelectors[j].addEventListener('click', function(){

				// Remove the visible from ALL selectors
				var activeSelectors = document.querySelectorAll('.visible');
				for (var k = activeSelectors.length - 1; k >= 0; k--) {

					// Made them invisible
					activeSelectors[k].classList.remove('visible');
				}

				// Find the selectors related to this button by getting it's class
				var selectors = document.querySelectorAll('[class*=M'+this.classList[0].slice(-2)+']');
				for (var k = selectors.length - 1; k >= 0; k--) {

					// Made them visible
					selectors[k].classList.add('visible');
				}
			});
		}
	};

</script>
<script src="/view/js/textarea.js"></script>
