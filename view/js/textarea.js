// Automatically resize textarea
function makeExpandingTextArea(textarea) {

	// Get the container
	var container = textarea.parentNode;

	// Add before the textarea the element that will get the text for sizing
	var pre = document.createElement('pre');
	var span = document.createElement('span'); pre.appendChild(span);
	var br = document.createElement('br'); pre.appendChild(br);
	container.insertBefore(pre, textarea);

	// For each of them add an input event to resize them automatically
	textarea.addEventListener('input', function() {
		span.textContent = textarea.value;
	}, false);

	// Default behaviour to resize the textarea
	span.textContent = textarea.value;

	// Check if the textarea should stay fixed
	if(textarea.className !== "fixed"){

		// Enable extra CSS
		container.className += " active";
	}
}

// Get all the fieldset
var textareas = document.getElementsByTagName('textarea');
var l = textareas.length; while (l--){
	makeExpandingTextArea(textareas[l]);
}
