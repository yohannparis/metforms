<?php
	header('content-type:text/html; charset=utf-8');
?><!DOCTYPE html>
<html lang="en-EN">
<head>
	<title>MET Forms</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="stylesheet" media="not print" type="text/css" href="/view/css/prod.css?v=2015-05-08">
	<link rel="stylesheet" media="print" type="text/css" href="/view/css/print.css">
	<link rel="icon" type="image/png" href="/model/images/favicon.png">
</head>
<body class="<?=$pageType;?>">
	<?php include 'view/pages/'.$page.'.php'; ?>
	<a class="footer" href="http://metricksystem.com">&copy; <?=date('Y');?>, The Metrick System Inc.</a>
	<script async src="//cdn.polyfill.io/v1/polyfill.js"></script>
	<?php /*
	<!--[if lt IE 9]>
		<script async src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script async src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->
	*/ ?>
</body>
</html>
